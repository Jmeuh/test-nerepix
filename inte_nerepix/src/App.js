import ButtonTop from "./components/ButtonTop";
import Contact from "./components/Contact";
import FavoriteToys from "./components/FavoriteToys";
import Footer from "./components/Footer";
import Header from "./components/Header";
import PixensCats from "./components/PixensCats";
import Presentation from "./components/Presentation";

function App() {
  return (
    <div className="App">
      <Header />
      <Presentation />
      <FavoriteToys />
      <PixensCats />
      <Contact />
      <Footer />
      <ButtonTop />
    </div>
  );
}

export default App;
