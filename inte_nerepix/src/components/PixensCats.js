import React from 'react';
import { cats } from '../models/cats';

const PixensCats = () => {
    return (
        <section id='cats' className='container pixens-cats f-col f-center'>
            <div className='f-center'>
                <hr></hr><h2>LES CHATS DES PIXENS</h2><hr></hr>
            </div>
            <div className='cat-list f-center f-wrap'>
                {cats.map((cat, key) => (
                    <div className='cat-div'>
                        <div className='cat-name'>{cat['name']}</div>
                        <img key={key} className='cat' src={cat['img']}/>
                    </div>
                ))}
            </div>
        </section>
    );
};

export default PixensCats;