import React from 'react';

const ToyCard = ({toy}) => {
    return (
        <div className='toy-card f-col f-between'>
            <div className='toy-img-content'><img src={toy['img']}/></div>
            <div className='toy-text'>
                <p className='toy-title'>{toy['name']}</p>
                <p className='toy-description'>{toy['description']}</p>
            </div>
        </div>
    );
};

export default ToyCard;