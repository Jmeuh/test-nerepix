import React from 'react';
import { sections } from '../models/sections.js';

const Navigation = () => {

    const goToSection = (pos) => {
        window.scrollTo(0,pos);
    }

    return (
        <ul className='navigation'>
            {sections.map((section, key) => (
                <li key={key} className='li-nav pointer' onClick={() => goToSection(section['pos'])}>
                    <a href={section['id']}>{section['title']}</a>
                </li>
            ))}
            <li className='li-nav pointer button contact-btn'>
                <a href='#contact' className='button'>ME CONTACTER</a>
            </li>
        </ul> 
    );
};

export default Navigation;