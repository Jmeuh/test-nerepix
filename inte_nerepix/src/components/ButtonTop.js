import React, { useEffect, useState } from 'react';

const ButtonTop = () => {

    const goToTop = () => {
        window.scrollTo(0,0);
    }

    const [top, setTop] = useState(0)

    useEffect(() => {
        window.addEventListener('scroll', ()=> {
            setTop(document.documentElement.scrollTop)
        })
    })

    return (
        <button className={top>650?'btnTopLink':'hidden'} onClick={goToTop}>
            Haut de page
        </button>
    );
};

export default ButtonTop;