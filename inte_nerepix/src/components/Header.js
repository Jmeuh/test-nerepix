import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useState } from 'react';
import Navigation from './Navigation';

const Header = () => {

    const [menu, setMenu] = useState(false)
    const menuToggle = () => {
        setMenu(!menu)
    }

    const [top, setTop] = useState(0)
    useEffect(() => {
        window.addEventListener('scroll', ()=> {
            setTop(document.documentElement.scrollTop)
        })
    })

    return (
        <div className={top>650?'small-header':'header'}>
            <h1>Ness Toys</h1>
            <div className='nav-content-off'>
                <Navigation />
            </div>
            <div className='small-nav'>
                <FontAwesomeIcon className='nav-icon' icon={faBars} onClick={menuToggle} />
                <div className={menu?'nav-content-on':'nav-content-off'} onClick={menuToggle}>
                    <Navigation />
                </div>
            </div>
        </div>
    );
};

export default Header;