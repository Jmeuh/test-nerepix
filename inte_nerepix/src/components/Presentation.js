import React from 'react';

const Presentation = () => {
    return (
        <section id='ness' className='container f-center prez-section'>
            <div className='f-center div-img'>
                <img className='prez-img' src='img/ness.png'/>
            </div>
            <div className='presentation'>
                <div className='prez-title'>
                    <h2>MOI C'EST NESS</h2>
                    <p>Le chat de l'agence</p>
                </div>
                <p className='prez-text'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </div>
        </section>
    );
};

export default Presentation;