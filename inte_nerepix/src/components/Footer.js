import React from 'react';

const Footer = () => {
    return (
        <div className='footer f-center'>
            <p>
                <strong>Ness Toys</strong>
            </p>
            <hr></hr>
            <p>Nerepix 2022</p>
        </div>
    );
};

export default Footer;