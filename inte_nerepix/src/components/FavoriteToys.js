import React, { useState } from 'react';
import { allToys } from '../models/allToys';
import { toys } from '../models/toys';
import ToyCard from './ToyCard';

const FavoriteToys = () => {

    const [all, setAll] = useState(false)
    const showAll = () => {
        if(!all) {
            setAll(true)
        }
        else {
            setAll(false)
        }
    }

    return (
        <section id='toys' className='container favorite-toys f-col f-center'>
            <div className='f-center'>
                <hr></hr><h2>MES JOUETS PRÉFÉRÉS !</h2><hr></hr>
            </div>
            <div className='f-center toy-list'>
                {toys.map((toy, key) => (
                    <ToyCard key={key} toy={toy} />
                ))}
            </div>
            <div className={all?'all-toys f-center toy-list f-wrap':'hidden'}>
                {allToys.map((toy, key) => (
                    <ToyCard key={key} toy={toy} />
                ))}
            </div>
            <a className='button pointer' onClick={showAll}>Voir tout mes jouets</a>
            
        </section>
    );
};

export default FavoriteToys;