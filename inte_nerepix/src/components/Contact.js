import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLocationDot } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faPhone } from '@fortawesome/free-solid-svg-icons';

const Contact = () => {
    return (
        <section className='container f-col f-center contact-section'>
            <div className='f-center'>
                <hr></hr><h2 id='contact'>ME CONTACTER</h2><hr></hr>
            </div>
            <div className='f-around contact-content'>
                <div>
                    <div className='f-row contact-item'>
                        <FontAwesomeIcon className='icon' icon={faLocationDot} />
                        <p>54 Avenue du Maréchal Croquette</p>
                    </div>
                    <div className='f-row contact-item'>
                        <FontAwesomeIcon className='icon' icon={faEnvelope} />
                        <p>ness@nerepix.fr</p>
                    </div>
                    <div className='f-row contact-item'>
                        <FontAwesomeIcon className='icon' icon={faPhone} />
                        <p>02 61 53 55 93</p>
                    </div>
                </div>
                <div className='ness-contact-div'>
                    <img className='ness-contact' src='../img/ness.png'/>
                </div>
            </div>
            
        </section>
    );
};

export default Contact;