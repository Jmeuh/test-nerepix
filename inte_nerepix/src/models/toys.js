export const toys = [
    {
        'name': 'La plume',
        'description': 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        'img': '../img/plume.png'
    },
    {
        'name': 'La souris',
        'description': 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        'img': '../img/souris.png'
    },
    {
        'name': 'Le poulet',
        'description': 'lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        'img': '../img/poulet.png'
    }
]