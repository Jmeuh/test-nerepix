export const cats = [
    {
        'name': 'PIXEL',
        'img': '../img/pixel.jpg',
    },
    {
        'name': 'SHELBY',
        'img': '../img/shelby.jpg',
    },
    {
        'name': 'MACHINE',
        'img': '../img/machine.jpg',
    },
    {
        'name': 'RIO',
        'img': '../img/rio.jpg',
    },
    {
        'name': 'CHOUPIE',
        'img': '../img/choupie.jpg',
    },
    {
        'name': 'ZOU',
        'img': '../img/zou.jpg',
    }
]